<?php

/**
 * @file
 *
 * Plugin to provide an argument handler for all entity ids.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t("Eck edit form: Entity type and ID"),
  'description' => t('Creates a ECK edit form context from a Entity ID argument.'),
  'context' => 'eck_pm_argument_entity_id_context',
  'get child' => 'eck_pm_argument_entity_id_get_child',
  'get children' => 'eck_pm_argument_entity_id_get_children',
  'default' => array(
    'entity_id' => '',
  ),
  'placeholder form' => 'eck_pm_argument_entity_id_eck_pm_argument_placeholder',
);

function eck_pm_argument_entity_id_get_child($plugin, $parent, $child) {
  $plugins = eck_pm_argument_entity_id_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

function eck_pm_argument_entity_id_get_children($original_plugin, $parent) {
  $entities = EntityType::loadAll();
  $plugins = array();
  foreach ($entities as $entity_type => $entity_object) {
    $entity = entity_get_info($entity_type);
    $plugin = $original_plugin;
    $plugin['title'] = t('@entity edit form: @entity_type ID', array('@entity' => $entity['label'], '@entity_type' => $entity_type));
    $plugin['keyword'] = $entity_type;
    $plugin['description'] = t('Creates a ECK edit form context from a @entity ID argument.', array('@entity' => $entity_type));
    $plugin['name'] = $parent . ':' . $entity_type;
    $plugin_id = $parent . ':' . $entity_type;
    $plugins[$plugin_id] = $plugin;
  }

  return $plugins;
}

/**
 * Discover if this argument gives us the entity we crave.
 */
function eck_pm_argument_entity_id_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  $entity_type = explode(':', $conf['name']);
  $entity_type = $entity_type[1];
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('eck_edit_form:' . $entity_type);
  }

  // We can accept either an entity object or a pure id.
  if (is_object($arg)) {
    return ctools_context_create('eck_edit_form:' . $entity_type, $arg);
  }

  // Trim spaces and other garbage.
  $arg = trim($arg);

  if (!is_numeric($arg)) {
    return FALSE;
  }

  $entities = entity_load($entity_type, array($arg));
  if (empty($entities)) {
    return FALSE;
  }

  return ctools_context_create('eck_edit_form:' . $entity_type, reset($entities));
}

function eck_pm_argument_entity_id_settings_form(&$form, &$form_state, $conf) {
  $plugin = &$form_state['plugin'];

  $form['settings']['entity'] = array(
    '#title' => t('Enter the title or ID of a @entity entity', array('@entity' => $plugin['keyword'])),
    '#type' => 'textfield',
    '#maxlength' => 512,
    '#autocomplete_path' => 'ctools/autocomplete/' . $plugin['keyword'],
    '#weight' => -10,
  );

  if (!empty($conf['entity_id'])) {
    $info = entity_load($plugin['keyword'], array($conf['entity_id']));
    $info = $info[$conf['entity_id']];
    if ($info) {
      $entity = entity_get_info($plugin['keyword']);
      $uri = entity_uri($plugin['keyword'], $info);
      if (is_array($uri) && $entity['entity keys']['label']) {
        $link = l(t("'%title' [%type id %id]", array('%title' => $info->{$entity['entity keys']['label']}, '%type' => $plugin['keyword'], '%id' => $conf['entity_id'])), $uri['path'], array('attributes' => array('target' => '_blank', 'title' => t('Open in new window')), 'html' => TRUE));
      }
      elseif (is_array($uri)) {
        $link = l(t("[%type id %id]", array('%type' => $plugin['keyword'], '%id' => $conf['entity_id'])), $uri['path'], array('attributes' => array('target' => '_blank', 'title' => t('Open in new window')), 'html' => TRUE));
      }
      elseif ($entity['entity keys']['label']) {
        $link = l(t("'%title' [%type id %id]", array('%title' => $info->{$entity['entity keys']['label']}, '%type' => $plugin['keyword'], '%id' => $conf['entity_id'])), file_create_url($uri), array('attributes' => array('target' => '_blank', 'title' => t('Open in new window')), 'html' => TRUE));
      }
      else {
        $link = t("[%type id %id]", array('%type' => $plugin['keyword'], '%id' => $conf['entity_id']));
      }
      $form['settings']['entity']['#description'] = t('Currently set to !link', array('!link' => $link));
    }
  }

  $form['settings']['entity_id'] = array(
    '#type' => 'value',
    '#value' => isset($conf['entity_id']) ? $conf['entity_id'] : '',
  );

  $form['settings']['entity_type'] = array(
    '#type' => 'value',
    '#value' => $plugin['keyword'],
  );

  return $form;
}

function eck_pm_argument_entity_id_eck_pm_argument_placeholder($conf) {
  $conf = array(
    '#title' => t('Enter the title or ID of a @entity entity', array('@entity' => $conf['keyword'])),
    '#type' => 'textfield',
    '#maxlength' => 512,
    '#autocomplete_path' => 'ctools/autocomplete/' . $conf['keyword'],
    '#weight' => -10,
  );

  return $conf;
}
